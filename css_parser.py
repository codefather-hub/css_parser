import re
import json
import logging

class Parser:
    
    def __init__(self, path=None, debug=True):
        '''
        param: debug = bool (non required. Default True)
        param: path = str (required). [path to css file]
        '''
        self.debug = debug
        if not path:
            return self.logger("Required path to css file")
        self.path = path
        self.file = open(path, 'r').read()
        if self.debug:
            self.logger(f"Init css file at {self.path}")

    def logger(self, msg: str = None):
        if self.debug:
            print(f"[CSS PARSER]: {msg}")

    def body(self):
        '''
        returned a body of css file
        '''
        print(self.file)

    def get(self, selector: str, finder: str):
        '''
        param: selector (example .body)
        param: finder (example background-image -> returned url)

        example:
        .body {
            background-image: url("/google.com/some_image.png");
        }

        with ".body" selector and "background-image" finder? its -
        its returned url("/google.com/some_image.png")
        '''

        css = [css for css in self.file.split("}\n") if css]
        css = json.loads(json.dumps({i.split("{")[0].strip():i.split("{")[1].strip() for i in css}))

        try:
            lines = css[selector].split(";")
            for line in lines:
                splited_line = line.split(":")
                if finder in line:
                    if self.debug:
                        self.logger(splited_line[1])
                    return splited_line[1]
        except:
            return None
